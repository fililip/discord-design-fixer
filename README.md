# About Discord Design Fixer
(yes ik very creative name)

**This Chromium-compatible extension gives you control over the message padding and allows you to tweak a few other things.**

# How to install

**THIS EXTENSION IS CHROMIUM-ONLY!! (works with browsers like Opera, Vivaldi and Brave) FIREFOX/SAFARI/EDGE _ARE_ AND _WILL_ BE UNSUPPORTED!!!**

Open the **Extensions** menu in your browser as shown below:

![alt text](screenshots/open_menu.png "Open the menu")

Then, load the extension using the **Load unpacked** button:

![alt text](screenshots/load_unpacked.png "Load the extension")

and select the extension's directory.

Refresh discord's tab and you're ready to go!

**Note to windows users: chrome might give you a warning message about developer mode being enabled _EVERY TIME YOU LAUNCH THE BROWSER!!_**
Unfortunately, there is _nothing_ I can do about it ~~except paying google to host the extension on their "web store" thing~~ and so to get rid of it you need to use Chromium [(link)](https://download-chromium.appspot.com/).

# Features

- change the message padding
- get rid of the recently used reaction emojis
- remove the "NEW" message pill

# How it looks like (in practice)
This is probably the most important thing this extension does - **_make more messages fit on the screen_**, even with cozy mode.
So here's how it looks like.

![alt text](screenshots/padding.png "Fix")

# Settings design (taste matters)

![alt text](screenshots/settings.png "Settings")

Yes, you **can** change the padding value to a **different** one. ~~_But why??_~~

# Addendum

There's a problem with free speech on the internet.
I can't say anything bad about something that _is_ bad, no matter _how_ bad it is.

Because if I do, I'll get banned by the people who made it bad.


Sad, isn't it?