let paddingSlider = document.querySelector("#padding-slider");
let paddingInfo = document.querySelector("#padding-info");
let paddingInfoContainer = document.querySelector("#padding-info-container");

let emojiCheckbox = document.querySelector("#emoji-checkbox");
let newCheckbox = document.querySelector("#new-checkbox");
let hoverCheckbox = document.querySelector("#hover-checkbox");
let activeCheckbox = document.querySelector("#active-checkbox");

let saveButton = document.querySelector("#save-button");

function load() {
    chrome.storage.sync.get({
        padding: "0",
        emoji: false,
        new: false,
        hover: false,
        active: true
    }, e => {
        paddingSlider.value = e.padding;
        paddingInfo.innerText = paddingSlider.value;

        if (e.emoji)
            emojiCheckbox.classList.add("checkbox-active");
        if (e.new)
            newCheckbox.classList.add("checkbox-active");
        if (e.hover)
            hoverCheckbox.classList.add("checkbox-active");
        if (e.active)
            activeCheckbox.classList.add("checkbox-active");
        else {
            for (let x of document.querySelectorAll(".checkbox")) {
                if (x != activeCheckbox)
                    x.classList.add("checkbox-disabled");
            }
            paddingSlider.disabled = true;
            paddingSlider.style.opacity = 0.4;
            paddingInfoContainer.style.opacity = 0.4;
        }
    });
}

function save() {
    chrome.storage.sync.set({
        padding: paddingSlider.value,
        emoji: emojiCheckbox.classList.contains("checkbox-active"),
        new: newCheckbox.classList.contains("checkbox-active"),
        hover: hoverCheckbox.classList.contains("checkbox-active"),
        active: activeCheckbox.classList.contains("checkbox-active")
    }, () => {});
}

document.addEventListener("DOMContentLoaded", load);

paddingSlider.addEventListener("input", () => {
    paddingInfo.innerText = paddingSlider.value;
});

saveButton.addEventListener("click", save);

document.addEventListener("selectstart", e => {
    e.preventDefault();
});

window.addEventListener("contextmenu", e => {
    e.preventDefault();
});

for (let x of document.querySelectorAll(".checkbox"))
    x.addEventListener("click", () => {
        if (x == activeCheckbox) {
            x.classList.toggle("checkbox-active");

            if (x.classList.contains("checkbox-active")) {
                for (let y of document.querySelectorAll(".checkbox")) {
                    if (y != activeCheckbox)
                        y.classList.remove("checkbox-disabled");
                }
                paddingSlider.disabled = false;
                paddingSlider.style.opacity = null;
                paddingInfoContainer.style.opacity = null;
            } else {
                for (let y of document.querySelectorAll(".checkbox")) {
                    if (y != activeCheckbox)
                        y.classList.add("checkbox-disabled");
                }
                paddingSlider.disabled = true;
                paddingSlider.style.opacity = 0.4;
                paddingInfoContainer.style.opacity = 0.4;
            }
        } else {
            if (activeCheckbox.classList.contains("checkbox-active"))
                x.classList.toggle("checkbox-active");
        }
    });