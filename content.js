chrome.storage.sync.get({
    padding: "0",
    emoji: false,
    new: false,
    hover: false,
    active: true
}, e => {
    let style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = chrome.extension.getURL("styles.css");

    (document.head || document.documentElement).appendChild(style);

    let stylesheet = document.styleSheets[document.styleSheets.length - 1];

    if (e.active) {
        stylesheet.insertRule(`.container-3FojY8 { padding: ${e.padding}rem 16px ${e.padding}rem 72px !important }`, stylesheet.cssRules.length);
        stylesheet.insertRule(`.wrapper-2a6GCs { padding: ${e.padding}rem 16px ${e.padding}rem 72px !important }`, stylesheet.cssRules.length);
        stylesheet.insertRule(`.zalgo-jN1Ica { padding: ${e.padding}rem 16px ${e.padding}rem 72px !important }`, stylesheet.cssRules.length);
        stylesheet.insertRule(`.group-spacing-16 .groupStart-23k01U { margin-top: 12px !important }`, stylesheet.cssRules.length);

        if (e.emoji) {
            stylesheet.insertRule(".emojiButton-jE9tXC { display: none !important }", stylesheet.cssRules.length);
            stylesheet.insertRule(`.button-1ZiXG9[aria-controls="popup_104"] { display: none !important }`, stylesheet.cssRules.length);
        }
        
        if (e.new)
            stylesheet.insertRule(".unreadPill-2HyYtt { display: none !important }", stylesheet.cssRules.length);
        if (e.hover)
            stylesheet.insertRule(".theme-dark {--background-message-hover: transparent !important}", stylesheet.cssRules.length);
    }
});